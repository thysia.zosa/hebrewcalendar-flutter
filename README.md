# hebrewCalendar – Flutter Edition

A hebrew Calendar with Dart / Flutter

## Idea
* HebrewDateTime-class: extension of DateTime with hebrew calendar fields (year, month, dayOfMonth)
* HebrewDatePicker-class: hebrew calendar equivalent to CupertinoDateTimePicker (date only, no time)
* a bit extravagant: automated prayer book...
