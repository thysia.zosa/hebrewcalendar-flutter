import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'screens/calendar_screen.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CupertinoApp(
      title: 'Hebrew Calendar',
      theme: CupertinoThemeData(
        barBackgroundColor: Colors.blue,
        primaryColor: Colors.blue,
        textTheme: CupertinoTextThemeData(primaryColor: Colors.white),
      ),
      home: CalendarScreen(),
    );
  }
}
