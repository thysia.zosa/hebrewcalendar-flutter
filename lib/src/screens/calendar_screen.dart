import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../widgets/cupertino_hebrew_date_picker.dart';
import '../models/hebrew_date.dart';

class CalendarScreen extends StatefulWidget {
  @override
  _CalendarScreenState createState() => _CalendarScreenState();
}

class _CalendarScreenState extends State<CalendarScreen> {
  DateTime _gregorianDate;
  HebrewDate _hebrewDate;

  @override
  void initState() {
    _gregorianDate = DateTime.now();
    _hebrewDate = HebrewDate.fromDateTime(_gregorianDate);
    super.initState();
  }

  _setGregorianDate(DateTime dateTime) {
    setState(() {
      _gregorianDate = dateTime;
      _hebrewDate = HebrewDate.fromDateTime(dateTime);
    });
  }

  _setHebrewDate(HebrewDate hebrewDate) {
    setState(() {
      _hebrewDate = hebrewDate;
      _gregorianDate = hebrewDate.toDateTime();
    });
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      navigationBar: CupertinoNavigationBar(
        middle: Text(
          'Calendar Picker',
          style: TextStyle(
            color: Colors.white,
          ),
        ),
      ),
      // child: Center(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Divider(),
          SizedBox(
            height: 100.0,
            child: CupertinoDatePicker(
              mode: CupertinoDatePickerMode.date,
              initialDateTime: _gregorianDate,
              minimumDate: null,
              onDateTimeChanged: (DateTime dateTime) {
                _setGregorianDate(dateTime);
              },
            ),
          ),
          Divider(),
          SizedBox(height: 20.0),
          Divider(),
          SizedBox(
            height: 100.0,
            child: CupertinoHebrewDatePicker(
              initialDate: _hebrewDate,
              onDateChanged: (HebrewDate hebrewDate) {
                _setHebrewDate(hebrewDate);
              },
            ),
          ),
        ],
      ),
      // ),
      //   ),
      // ),
      //   ),
      // ),
    );
  }
}
