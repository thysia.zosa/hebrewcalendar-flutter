import 'package:hebrewCalendar/src/constants.dart';

class HebrewDateTime extends DateTime {
  // Hebrew Month constants that are returned by the [hebrewMonth] getter.
  static const int nisan = 1;
  static const int iyar = 2;
  static const int siwan = 3;
  static const int thammuz = 4;
  static const int av = 5;
  static const int elul = 6;
  static const int theshri = 7;
  static const int cheshwan = 8;
  static const int kislew = 9;
  static const int teveth = 10;
  static const int shevat = 11;
  static const int adar = 12;
  static const int adarRishon = 12;
  static const int adarSheni = 13;

  int _julianDate;
  int _hebrewYear;
  int _hebrewMonth;
  int _hebrewDayInMonth;

  int get hebrewYear => _hebrewYear;
  int get hebrewMonth => _hebrewMonth;
  int get hebrewDayInMonth => _hebrewDayInMonth;

  HebrewDateTime(int hebrewYear, [hebrewMonth = 1, hebrewDay = 1])
      : this._hebrewYear = hebrewYear,
        this._hebrewMonth = hebrewMonth,
        this._hebrewDayInMonth = hebrewDay,
        // TODO: julianDate etc.
        super(hebrewYear - 3760);

  HebrewDateTime.fromGregorian(
    int year, [
    int month = 1,
    int day = 1,
  ]) : super(year, month, day) {
    this._julianDate = _julianDateFromGregorian(year, month, day);
    List<int> _hebrewDate = _hebrewDateFromJulianDate(_julianDate);
    this._hebrewYear = _hebrewDate[0];
    this._hebrewMonth = _hebrewDate[1];
    this._hebrewDayInMonth = _hebrewDate[2];
  }

  int _julianDateFromGregorian(int year, int month, int dayInMonth) {
    int _monthNumber = month < 3 ? month + 9 : month - 3;
    int _yearNumber = _monthNumber < 10 ? year : year - 1;
    int _julianDateNumber = (_yearNumber * 365.25).floor() +
        (_monthNumber * 30.6 + 0.5).floor() +
        dayInMonth +
        1721117;
    _julianDateNumber = _julianDateNumber > 2299170
        ? _julianDateNumber +
            2 +
            (_yearNumber / 400).floor() -
            (_yearNumber / 100).floor()
        : _julianDateNumber;
    return _julianDateNumber;
  }

  List<int> _hebrewDateFromJulianDate(int julianDate) {
    int _hebrewEpochDate = julianDate - 347997;
    int _monthNumber = (_hebrewEpochDate / hebrewMonthLength).floor();
    int _cyclesNumber = ((_monthNumber - 1) / 235).floor();
    _monthNumber -= _cyclesNumber * 235;
    int _year = _cyclesNumber * 19;
    _year += ((_monthNumber + 0.94) / hebrewYearInMonths).floor() + 1;

    int _beforeNewYear = _roshHashanaEven(_year);

    if (_beforeNewYear >= _hebrewEpochDate) {
      _year--;
      _beforeNewYear = _roshHashanaEven(_year);
    }

    int _thisYearsDay = _hebrewEpochDate - _beforeNewYear;
    int _yearLength = _roshHashanaEven(_year + 1) - _beforeNewYear;
    int _dayNumber = (_thisYearsDay + 205).remainder(_yearLength);

    if (_dayNumber < 265) {
      _dayNumber++;
      _monthNumber = (_dayNumber / 29.55).floor();
      _dayNumber -= (_monthNumber * 29.5).floor();
    } else {
      _dayNumber -= 264;
      switch (_yearLength) {
        case 353:
        case 383:
          _monthNumber = ((_dayNumber + 0.15) / 29.55).floor();
          _dayNumber -= (_monthNumber * 29.4).floor();
          _monthNumber += 9;
          break;
        case 354:
        case 384:
          _monthNumber = ((_dayNumber - 0.95) / 29.52).floor();
          _dayNumber -= (_monthNumber * 29.5 + 0.9).floor();
          _monthNumber += 9;
          break;
        case 355:
        case 385:
          _monthNumber = ((_dayNumber - 1.95) / 29.52).floor() + 1;
          _dayNumber -= (_monthNumber * 19.6 + 0.9).floor();
          _monthNumber += 8;
          break;
      }
    }

    if (_monthNumber == 0) {
      if (_isLeapYear(_year)) {
        _monthNumber = 13;
      } else {
        _monthNumber = 12;
      }
    }

    return [_year, _monthNumber, _dayNumber];
  }

  int _roshHashanaEven(int year) {
    int _yearsNumber = year - 1;
    int _cyclesNumber = (_yearsNumber / 19).floor();
    int _cycleYearNumber = _yearsNumber.remainder(19);
    int _cycleMonthNumber =
        _cycleYearNumber * 12 + (_cycleYearNumber * 0.37 + 0.06).floor();
    int _chalaqimNumber =
        _cyclesNumber * 17875 + _cycleMonthNumber * 13753 + 5604;
    int _fullDayNumber = _cyclesNumber * 6939 +
        _cycleMonthNumber * 29 +
        (_chalaqimNumber / 25920).floor();
    _chalaqimNumber = _chalaqimNumber.remainder(25920);
    int _weekDayHelper = _fullDayNumber.remainder(7);

    if (_isLeapYear(_yearsNumber) &&
        _weekDayHelper == 0 &&
        _chalaqimNumber >= 16789) {
      _chalaqimNumber = 19440;
    }

    if (!_isLeapYear(_yearsNumber + 1) &&
        _weekDayHelper == 1 &&
        _chalaqimNumber >= 9924) {
      _chalaqimNumber = 19940;
    }

    if (_chalaqimNumber >= 19440) {
      _fullDayNumber++;
    }

    _weekDayHelper = _fullDayNumber.remainder(7);
    if (_weekDayHelper == 2 || _weekDayHelper == 4 || _weekDayHelper == 6) {
      _fullDayNumber++;
    }

    return _fullDayNumber;
  }

  bool _isLeapYear(int year) {
    switch ((year - 1).remainder(19) + 1) {
      case 3:
      case 6:
      case 8:
      case 11:
      case 14:
      case 17:
      case 19:
        return true;
        break;
      default:
        return false;
    }
  }

  // : super(year);
  // HebrewDateTime.fromMicrosecondsSinceEpoch(int year)
  //     : super.fromMicrosecondsSinceEpoch(year);
  // HebrewDateTime.fromMillisecondsSinceEpoch(int year)
  //     : super.fromMillisecondsSinceEpoch(year);
  // HebrewDateTime.utc(int year) : super.utc(year);
  // HebrewDateTime.now() : super.now();
}
