import '../constants.dart';

class HebrewDate {
  static int _occidentalDay;
  static int _occidentalMonth;
  static int _occidentalYear;
  static int _julianDate;
  static int _hebrewDay;
  static int _hebrewMonth;
  static int _hebrewYear;
  static int _weekDayNumber;

  static final List<String> _weekDays = [
    'Sonntag',
    'Montag',
    'Dienstag',
    'Mittwoch',
    'Donnerstag',
    'Freitag',
    'Samstag',
  ];

  static final List<String> _hebrewMonths = [
    'Adar',
    'Nisan',
    'Iyyar',
    'Siwan',
    'Tammuz',
    'Av',
    'Elul',
    'Teschri',
    'Cheschwan',
    'Kislew',
    'Tevet',
    'Schvat',
    'Adar Rischon',
    'Adar Scheni',
  ];

  static final List<String> _occidentalMonths = [
    'Januar',
    'Februar',
    'März',
    'April',
    'Mai',
    'Juni',
    'Juli',
    'August',
    'September',
    'Oktober',
    'November',
    'Dezember',
  ];

  // HebrewDate(int year, [int month = 1, int day = 1])
  //     : this._internal(year, month, day);

  //     HebrewDate._internal(int year, int month, int day)

  HebrewDate.fromDateTime(DateTime dateTime) {
    _occidentalYear = dateTime.year;
    _occidentalMonth = dateTime.month;
    _occidentalDay = dateTime.day;
    _occidentalToJulianDate();
    _julianDateWeekDay();
    _julianDateToHebrew();
  }

  DateTime toDateTime() {
    return DateTime(_occidentalYear, _occidentalMonth, _occidentalDay);
  }

  String get weekDay => _weekDays[_weekDayNumber];
  int get hebrewDay => _hebrewDay;
  int get hebrewMonth => _hebrewMonth == 0 ? 12 : _hebrewMonth;
  int get hebrewYear => _hebrewYear;

  String get hebrewDate {
    String _yearinCounting;
    if (_hebrewYear < 1) {
      _yearinCounting = (_hebrewYear - 1).abs().toString() + " v. d. Z.";
    } else {
      _yearinCounting = _hebrewYear.toString() + " d. Z.";
    }
    return _hebrewDay.toString() +
        ". " +
        _hebrewMonths[_hebrewMonth] +
        " " +
        _yearinCounting;
  }

  String get occidentalDate {
    String _yearinCounting;
    if (_occidentalYear < 1) {
      _yearinCounting = (_occidentalYear - 1).abs().toString() + " v. Chr.";
    } else {
      _yearinCounting = _occidentalYear.toString() + " n. Chr.";
    }
    return _occidentalDay.toString() +
        ". " +
        _occidentalMonths[_occidentalMonth - 1] +
        " " +
        _yearinCounting;
  }

  String get shortDate =>
      _occidentalDay.toString() +
      ". " +
      _occidentalMonth.toString() +
      " " +
      _occidentalYear.toString();

  void _occidentalToJulianDate() {
    int _monthNumber =
        _occidentalMonth < 3 ? _occidentalMonth + 9 : _occidentalMonth - 3;
    int _yearNumber = _monthNumber < 10 ? _occidentalYear : _occidentalYear - 1;
    int _julianDateNumber = (_yearNumber * 365.25).floor() +
        (_monthNumber * 30.6 + 0.5).floor() +
        _occidentalDay +
        1721117;
    _julianDateNumber = _julianDateNumber > 2299170
        ? _julianDateNumber +
            2 +
            (_yearNumber / 400).floor() -
            (_yearNumber / 100).floor()
        : _julianDateNumber;
    _julianDate = _julianDateNumber;
  }

  void _julianDateWeekDay() {
    _weekDayNumber = (_julianDate + 1).remainder(7);
  }

  void _julianDateToHebrew() {
    int _hebrewEpochDate = _julianDate - 347997;
    int _monthNumber = (_hebrewEpochDate / hebrewMonthLength).floor();
    int _cyclesNumber = ((_monthNumber - 1) / 235).floor();
    _monthNumber -= _cyclesNumber * 235;
    int _year = _cyclesNumber * 19;
    _year += 1 + ((_monthNumber + 0.94) / hebrewYearInMonths).floor();

    int _beforeNewYear = _roshHashanaEven(_year);

    if (_beforeNewYear >= _hebrewEpochDate) {
      _year--;
      _beforeNewYear = _roshHashanaEven(_year);
    }
    int _thisYearsDay = _hebrewEpochDate - _beforeNewYear;

    int _yearLength = _roshHashanaEven(_year + 1) - _beforeNewYear;
    int _dayNumber = (_thisYearsDay + 205).remainder(_yearLength);

    if (_dayNumber < 265) {
      _dayNumber++;
      _monthNumber = (_dayNumber / 29.55).floor();
      _dayNumber -= (_monthNumber * 29.5).floor();
    } else {
      _dayNumber -= 264;
      switch (_yearLength) {
        case 353:
        case 383:
          _monthNumber = ((_dayNumber + 0.15) / 29.55).floor();
          _dayNumber -= (_monthNumber * 29.4).floor();
          _monthNumber += 9;
          break;
        case 354:
        case 384:
          _monthNumber = ((_dayNumber - 0.95) / 29.52).floor();
          _dayNumber -= (_monthNumber * 29.5 + 0.9).floor();
          _monthNumber += 9;
          break;
        case 355:
        case 386:
          _monthNumber = ((_dayNumber - 1.95) / 29.52).floor() + 1;
          _dayNumber -= (_monthNumber * 29.6 + 0.9).floor();
          _monthNumber += 8;
          break;
        // default:
      }
    }

    if (_isLeapYear(_year) && _monthNumber == 0) {
      _monthNumber = 13;
    }

    _hebrewYear = _year;
    _hebrewMonth = _monthNumber;
    _hebrewDay = _dayNumber;
  }

  int _roshHashanaEven(int year) {
    int _yearsNumber = year - 1;
    int _cyclesNumber = (_yearsNumber / 19).floor();
    int _cycleYearNumber = _yearsNumber.remainder(19);
    int _cycleMonthNumber =
        _cycleYearNumber * 12 + (_cycleYearNumber * 0.37 + 0.06).floor();
    int _chalaqimNumber =
        _cyclesNumber * 17875 + _cycleMonthNumber * 13753 + 5604;
    int _fullDayNumber = _cyclesNumber * 6939 +
        _cycleMonthNumber * 29 +
        (_chalaqimNumber / 25920).floor();
    _chalaqimNumber = _chalaqimNumber.remainder(25920);
    int _weekDayHelper = _fullDayNumber.remainder(7);

    if (_isLeapYear(_yearsNumber) &&
        _weekDayHelper == 0 &&
        _chalaqimNumber >= 16789) {
      _chalaqimNumber = 19440;
    }

    if (!_isLeapYear(_yearsNumber + 1) &&
        _weekDayHelper == 1 &&
        _chalaqimNumber >= 9924) {
      _chalaqimNumber = 19440;
    }

    if (_chalaqimNumber >= 19440) {
      _fullDayNumber++;
    }

    _weekDayHelper = _fullDayNumber.remainder(7);
    if (_weekDayHelper == 2 || _weekDayHelper == 4 || _weekDayHelper == 6) {
      _fullDayNumber++;
    }

    return _fullDayNumber;
  }

  bool _isLeapYear(int year) {
    switch ((year - 1).remainder(19) + 1) {
      case 3:
      case 6:
      case 8:
      case 11:
      case 14:
      case 17:
      case 19:
        return true;
      default:
        return false;
    }
  }
}
